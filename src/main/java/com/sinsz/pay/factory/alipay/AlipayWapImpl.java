package com.sinsz.pay.factory.alipay;

import com.sinsz.pay.properties.PayProperties;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO 支付宝支付实现
 * @author chenjianbo
 */
public class AlipayWapImpl extends BaseAlipayImpl {


    AlipayWapImpl(PayProperties prop, HttpServletRequest request) {
        super(prop, request);
    }

    @Override
    public String unifiedOrder(String outTradeNo, String body, String detail, int fee, String openid) {
        return null;
    }
}

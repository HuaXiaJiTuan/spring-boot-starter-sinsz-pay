package com.sinsz.pay.support;

/**
 * 支付宝全局常量
 * @author chenjianbo
 */
public interface AliPayConstant extends Constant {

    /**
     * 支付网关
     */
    String GATEWAY = "https://openapi.alipay.com/gateway.do";

    /**
     * 支付宝 - 超时时间
     */
    String ALI_ORDER_EXPIRATION_INTERVAL = ORDER_EXPIRATION_INTERVAL + "m";

    /**
     * 支付宝编码
     */
    String CHARSET = "UTF-8";

    /**
     * 支付宝返回格式
     */
    String FORMAT = "json";

    /**
     * 支付宝RSA2
     */
    String SIGNTYPE = "RSA2";

    /**
     * 产品代码
     */
    String PRODUCT_CODE = "QUICK_MSECURITY_PAY";

    /**
     * 支付宝收款账号类型
     * <p>
     *     ALIPAY_USERID：支付宝账号对应的支付宝唯一用户号。以2088开头的16位纯数字组成。
     *     ALIPAY_LOGONID：支付宝登录号，支持邮箱和手机号格式。
     * </p>
     */
    String PAYEE_TYPE = "ALIPAY_LOGONID";
}
